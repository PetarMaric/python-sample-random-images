from typing import List
import os
import random
from pathlib import Path
import shutil


class DirectoryNotFoundException(Exception):
    pass


def sample_random_images(
        *,
        source_directory: str,
        output_directory: str,
        supported_image_formats: List[str],
        number_of_random_images_to_copy: int
):
    if number_of_random_images_to_copy <= 0:
        raise ValueError("\"number_of_random_images_to_copy\" can not be equal to or less than 0.")

    if not Path(source_directory).exists():
        raise DirectoryNotFoundException(f"Source directory \"{source_directory}\" not found.")

    if not Path(output_directory).exists():
        raise DirectoryNotFoundException(
            f"Output directory \"{output_directory}\" not found."
        )

    absolute_image_paths = list()

    for directory_path, subdirectories, filenames in os.walk(source_directory):
        for filename in filenames:
            file_extension = filename.split('.')[-1]
            if file_extension not in supported_image_formats:
                continue

            absolute_image_path = Path(directory_path, filename)
            absolute_image_paths.append(absolute_image_path)

    print(f"Found potential {len(absolute_image_paths)} images.")

    if len(absolute_image_paths) < number_of_random_images_to_copy:
        raise ValueError("Number of random samples is greater than the available image population.")

    for image_path in random.sample(absolute_image_paths, number_of_random_images_to_copy):
        shutil.copy(src=image_path, dst=output_directory)
        print(f"Copied: \"{image_path}\" to {output_directory}")
