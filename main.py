from sample_random_images import sample_random_images

ROOT_IMAGES_DIRECTORY = r""
ROOT_OUTPUT_IMAGE_DIRECTORY = r""
SUPPORTED_IMAGE_FORMATS = ["png", "webp", "jpg"]
NUMBER_OF_RANDOM_IMAGES_TO_COPY = 0

sample_random_images(
    source_directory=ROOT_IMAGES_DIRECTORY,
    output_directory=ROOT_OUTPUT_IMAGE_DIRECTORY,
    supported_image_formats=SUPPORTED_IMAGE_FORMATS,
    number_of_random_images_to_copy=NUMBER_OF_RANDOM_IMAGES_TO_COPY
)
